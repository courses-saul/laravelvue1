<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Administracion\UsersController;
use App\Http\Controllers\Administracion\RolesController;
use App\Http\Controllers\FilesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/administracion/usuario/getListarUsuarios', [UsersController::class, 'getListarUsuarios']);
Route::post('/administracion/usuario/setRegistrarUsuario', [UsersController::class, 'setRegistrarUsuario']);
Route::post('/administracion/usuario/setEditarUsuario', [UsersController::class, 'setEditarUsuario']);
Route::post('/administracion/usuario/setCambiarEstadoUsuario', [UsersController::class, 'setCambiarEstadoUsuario']);
Route::get('/administracion/rol/getListarRoles', [RolesController::class, 'getListarRoles']);

Route::post('/archivo/setRegistrarArchivo', [FilesController::class, 'setRegistrarArchivo']);
Route::get('/v1/pruebaget', function() {
    return 'hola';
});

Route::get('/{optional?}', function () {
    return view('app');
}) -> name('basepath') -> where('optional', '.*');

