<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PayUService\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FilesController extends Controller
{
    public function setRegistrarArchivo(Request $request){
        $file = $request->file;
        
        // se genera un hash para evitar problenas de filenames en un futuro;
        $hash = Str::random(10);
        $filename = $file->getClientOriginalName();
        $fileserver = $hash .'_'. $filename;

        Storage::putFileAs('public/users', $file, $fileserver);

        $path = asset('storage/users/'.$fileserver);

        $resultDb = DB::select('call sp_Archivo_setRegistrarArchivo(?, ?)', [
            $path, $filename,
        ]);

        return $resultDb;
    }
}
